#!/bin/bash


txt2man -d "${CHANGELOG_DATE}" -t OSMO-SGSN              -s 8 osmo-sgsn.txt            > osmo-sgsn.8
txt2man -d "${CHANGELOG_DATE}" -t OSMO-GBPROXY           -s 8 osmo-gbproxy.txt         > osmo-gbproxy.8
txt2man -d "${CHANGELOG_DATE}" -t OSMO-GTPHUB            -s 8 osmo-gtphub.txt          > osmo-gtphub.8
